package nu.westlin.springcloudfeigndemo;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InMemoryCarRepository implements CarRepository {

    List<Car> cars = Lists.newArrayList(
        new Car("Porsche", "997 RSR"),
        new Car("Opel", "Ascona B 1,9S"));

    @Override public List<Car> getCars() {
        return cars;
    }
}

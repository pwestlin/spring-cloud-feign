package nu.westlin.springcloudfeigndemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {

    @GetMapping(value = "hello/{name}")
    public String sayHello(@PathVariable String name) {
        return "Hello " + name + "!";
    }

    @GetMapping(value = "404")
    public String notFound() {
        throw new ResourceNotFoundException();
    }

    @GetMapping(value = "500")
    public String internalServerError() {
        throw new RuntimeException("Crash!");
    }

}

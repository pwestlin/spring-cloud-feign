package nu.westlin.springcloudfeigndemo;

import java.util.List;

public interface CarRepository {

    List<Car> getCars();
}

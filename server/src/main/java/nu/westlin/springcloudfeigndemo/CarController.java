package nu.westlin.springcloudfeigndemo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    protected CarRepository carRepository;

    @Inject
    public CarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping(value = "list")
    public List<Car> list() {
        return carRepository.getCars();
    }

}

package nu.westlin.springcloudfeigndemo;

import feign.RequestInterceptor;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

    @Value("${server.username}")
    private String username;

    @Value("${server.password}")
    private String password;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new BasicAuthRequestInterceptor(username, password);
    }

/*
    @Bean
    public CustomErrorDecoder myErrorDecoder() {
        return new CustomErrorDecoder();
    }
*/
}

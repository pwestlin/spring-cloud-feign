package nu.westlin.springcloudfeigndemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableFeignClients
public class ClientApplication {

    protected static final Logger logger = LoggerFactory.getLogger(ClientApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(ClientApplication.class, args);

        FooService service = ctx.getBean(FooService.class);

        service.sayHello("Sune");

        service.doNotFind();
        service.internalError();
        service.listCars();
        service.listCars2();
    }
}

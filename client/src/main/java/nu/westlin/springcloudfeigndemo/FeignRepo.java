package nu.westlin.springcloudfeigndemo;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "server-service", url = "${server.baseUrl}")
public interface FeignRepo {

    @RequestMapping(method = RequestMethod.GET, value = "/hello/{name}")
    String sayHelloTo(@PathVariable("name") String name);

    @RequestMapping(method = RequestMethod.GET, value = "/404")
    void notFound();

    @RequestMapping(method = RequestMethod.GET, value = "/car/list")
    List<Car> listCars();

    @RequestMapping(method = RequestMethod.GET, value = "/car/list")
    ResponseEntity<List<Car>> listCars2();

    @RequestMapping(method = RequestMethod.GET, value = "/500")
    void internalError();

    @RequestMapping(method = RequestMethod.GET, value = "/hello/{name}")
    ResponseEntity<String> sayHelloTo2(String sune);
}

package nu.westlin.springcloudfeigndemo;

import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class FooServiceImpl implements FooService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private FeignRepo feignRepo;

    @Inject
    public FooServiceImpl(FeignRepo feignRepo) {
        this.feignRepo = feignRepo;
    }

    @Override public void sayHello(String name) {
        logger.info(feignRepo.sayHelloTo(name));
    }

    @Override public void doNotFind() {
        try {
            feignRepo.notFound();
        } catch (FeignException e) {
            if (e.status() == 404) {
                logger.error("Could not find resource - just like it was meant to.. :)");
            } else {
                logger.error("Error calling server", e);
            }
        }

    }

    @Override public void listCars() {
        this.logger.info("Cars:");
        feignRepo.listCars().forEach(car -> logger.info(car.toString()));
    }

    @Override public void listCars2() {
        //feignRepo.listCars2().forEach(car -> logger.info(car.toString()));
        ResponseEntity<List<Car>> entity = feignRepo.listCars2();
        this.logger.info("entity.getStatusCode() = {}", entity.getStatusCode());
        this.logger.info("Cars:");
        entity.getBody().forEach(car -> logger.info(car.toString()));
    }

    @Override public void internalError() {
        try {
            feignRepo.internalError();
        } catch (ServerInternalErrorException e) {
            logger.error("Error calling server - just like it was meant to.. :)", e);
        }
    }

}

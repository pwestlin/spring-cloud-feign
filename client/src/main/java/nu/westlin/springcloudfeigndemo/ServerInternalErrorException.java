package nu.westlin.springcloudfeigndemo;

import feign.Response;

public class ServerInternalErrorException extends RuntimeException {

    private final String methodKey;
    private final Response response;

    public ServerInternalErrorException(String methodKey, Response response) {
        super(response.reason() != null ? response.reason() : "");

        this.methodKey = methodKey;
        this.response = response;
    }

    @Override public String toString() {
        return "Internal server error while calling method '" + methodKey + "'. Error:\n" + response.toString();
    }
}

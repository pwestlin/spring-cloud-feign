package nu.westlin.springcloudfeigndemo;

import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;

public class CustomErrorDecoder implements ErrorDecoder {

    private final ErrorDecoder defaultErrorDecoder = new Default();

    @Override
    public Exception decode(String methodKey, Response response) {
        if (response.status() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            return new ServerInternalErrorException(methodKey, response);
        }

        return defaultErrorDecoder.decode(methodKey, response);
    }

}
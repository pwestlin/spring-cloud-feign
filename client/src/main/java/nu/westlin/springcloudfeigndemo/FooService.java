package nu.westlin.springcloudfeigndemo;

public interface FooService {

    void sayHello(String name);

    void doNotFind();

    void listCars();

    void listCars2();

    void internalError();

}

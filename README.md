Demo of [Spring Cloud Feign](http://projects.spring.io/spring-cloud/spring-cloud.html#spring-cloud-feign).

The demo has two parts, a server with some REST-services and a consuming client.

To try it:

1. Open console.

2. cd server

3. gradle bootrun

4. Open console.

5. cd client

6. gradle bootrun

7. Look for logs from class FooServiceImpl. :)